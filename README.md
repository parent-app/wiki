# WIKI

## Concept

Application destinée aux parents qui souhaitent avoir des informations sur la croissance des enfants (vaccins obligatoires, diversification alimentaire, étapes du développement), gérer leur agenda pour par exemple ajouter les différents rendez-vous pour leurs enfants, etc. 

## Technologies utilisées

**Front** : 
- Ionic 5
- Angular 10

**Back** : 
- Java 11
- Spring Boot 2.4.1 (Starter data Jpa, starter Security, starter Validation, starter Web)
- MapStruct 1.4.1.Final
- Project Lombok 1.18.20
- JJWT 0.9.1
- Flyway 7.1.1
- QueryDsl 4.1.3
- Spring Doc Open Api 1.5.0
- Jsoup 1.13.1
- Rome 1.8.0
- PostgreSQL 13.1

## Architecture Back

La partie Back est composée de 4 micro-services : 

- MS Gestion de compte : responsable de toute la gestion des comptes utilisateurs ainsi que de l'authentification
- MS Agenda : responsable de l'agenda des utilisateurs (gestion des rendez-vous)
- MS Croissance enfant : responsable de la partie croissance de l'enfant : vaccins, diversifacation alimentaire, étapes du développement
- MS Flux rss : pour la gestion des flux rss (ajout de flux, récupération et lecture des flux)

## Installer et exécuter l'application

### Via les scripts : 
  
Dans le dossier `scripts` : 

- `install-parentApp.sh` : ce script va créer, s'ils n'existent pas, les différents dossiers du chemin founi en paramètre, puis va cloner les projets (front et back), les installer, créer les containers docker pour les bases de données, installer les projets (`npm install` et `mvn install`).

- `create-ms-services.sh` : ce script va créer des services pour chaque micro-services. Vous pourrez ensuite les exécuter manuellement à l'aide de la commande `sudo service nom_service start`. 

- `run-parentApp-ms.sh` : ce script démarre les containers docker pour les bases de données des micro-services et démarre les micro-services 

- `stop-parentApp-ms.sh` : ce script arrête les containers docker ainsi que les services. Si vous voulez arrêter manuellement les services : `sudo service nom_service stop`. Pour les instances docker : `docker stop nom_container` 


Pour exécuter un script : `sh nom_du_script chemin_execution`. Le paramètre `chemin_execution` correspond au dossier dans lequel vous souhaitez installer le projet.
/!\ Pour les autres scripts, utiliser le même chemin que celui utilisé pour exécuter le script `install-parentApp.sh`.

Avant d'exécuter les scripts, vous devez avoir d'installés sur votre poste : 
- npm
- java jdk 11
- maven 3.6
- docker

/!\ Des commandes sont exécutées avec le mot-clé `sudo` : il est donc possible que le terminal vous demande votre mot-de-passe.

| Micro-service 	| Nom du service linux 	| Nom de l'instance docker pour la bdd 	|
|-	|-	|-	|
| MS Gestion compte 	| parentApp-gestion-compte 	| parentApp-gestion-compte 	|
| MS Agenda 	| parentApp-agenda 	| parentApp-agenda 	|
| MS Evolution enfant 	| parentApp-evolution-enfant 	| parentApp-evolution-enfant 	|
| MS Flux RSS 	| parentApp-flux-rss 	| parentApp-flux-rss 	|

Pour exécuter le front, il suffit d'aller dans le dosier du projet `client` et d'exécuter la commande `ionic serve`. Vous pourrez ensuite consulter l'application dans le navigateur à l'adresse suivante : `http://localhost:8100`.

Pour une meilleure expérience utilisateur, le mieux est de lancer l'application client avec un émulateur de smartphone dans Android Studio ([documentation android studio](https://developer.android.com/studio/run/emulator?gclid=CjwKCAjwz_WGBhA1EiwAUAxIcY0D0QeNZ7h6-vrDxea58ZplIFpB6jWEQasIW_V_J9ZRGExPzbMtJBoCNHIQAvD_BwE&gclsrc=aw.ds)).

Pour copier les assets web vers la plateforme souhaitée (android ou ios), vous devez exécuter la commande suivante dans le projet ionic : `ionic capacitor copy [platform]` (remplacer `[platform]` par ios ou android).
Ensuite, ajouter au fichier `AndroidManifest.xml`, dans la balise `<application>`, la ligne suivante : `android:usesCleartextTraffic="true"`;

/!\ Pour que l'application dans l'émulateur puisse requêter les micro-services, il convient de modifier le fichier de l'application parent : `src/app/constants.ts` et remplacer `localhost` par `10.0.2.2`. Ce qui donne par exemple : `http://localhost:8081/api` => `http://10.0.2.2:8081/api`

### Lancement manuel

#### 1. Cloner les projets

- `git clone https://gitlab.com/parent-app/flux-rss.git`
- `git clone https://gitlab.com/parent-app/evolution-enfant.git`
- `git clone https://gitlab.com/parent-app/gestion-compte.git`
- `git clone https://gitlab.com/parent-app/agenda.git`
- `git clone https://gitlab.com/parent-app/client.git`

#### 2. Créer les containers docker pour les bases de données

- `sudo docker run --name parentApp-agenda -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=parentApp_agenda -d -p 5433:5432 postgres`
- `sudo docker run --name parentApp-evolution-enfant -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=parentApp_croissance_enfant -d -p 5434:5432 postgres`
- `sudo docker run --name parentApp-gestion-compte -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=parentApp_gestion_compte -d -p 5432:5432 postgres`
- `sudo docker run --name parentApp-flux-rss -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=parentApp_rss_feeds -d -p 5435:5432 postgres`

#### 3. Installer les projets

- Commande à exécuter à la racine de chaque micro-service : `mvn install`

- Commande à exécuter à la racine du projet front (client) : `npm install`

#### 4. Lancer les projets 

- Commande à exécuter à la racine de chaque micro-service : `mvn spring-boot:run`

- Commande à exécuter à la racine du projet front (client) : `ionic serve`. Vous pourrez ensuite consulter l'application dans le navigateur à l'adresse suivante : `http://localhost:8100`.
Pour une meilleure expérience utilisateur, le mieux est de lancer l'application client avec un émulateur de smartphone dans Android Studio ([documentation android studio](https://developer.android.com/studio/run/emulator?gclid=CjwKCAjwz_WGBhA1EiwAUAxIcY0D0QeNZ7h6-vrDxea58ZplIFpB6jWEQasIW_V_J9ZRGExPzbMtJBoCNHIQAvD_BwE&gclsrc=aw.ds)).

/!\ Pour que l'application dans l'émulateur puisse requêter les micro-services, il convient de modifier le fichier de l'application parent : `src/app/constants.ts` et remplacer `localhost` par `10.0.2.2`. Ce qui donne par exemple : `http://localhost:8081/api` => `http://10.0.2.2:8081/api`.
Ensuite, ajouter au fichier `AndroidManifest.xml`, dans la balise `<application>`, la ligne suivante : `android:usesCleartextTraffic="true"`;

Pour copier les assets web vers la plateforme souhaitée (android ou ios), vous devez exécuter la commande suivante dans le projet ionic : `ionic capacitor copy [platform]` (remplacer `[platform]` par ios ou android).

### Liste des ports utilisés par l'application

| MS 	| Port 	|
|---	|---	|
| Gestion compte 	| 8081 	|
| Agenda 	| 8082 	|
| Croissance enfant 	| 8083 	|
| Flux RSS 	| 8084 	|
| Client 	| 8100 	|

## Utilisateurs par défaut

| Username 	| Password 	|
|-	|-	|
| admin 	| admin 	|
| user 	| user 	|
