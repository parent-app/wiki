### Stop docker instances ###
echo "Arrêt des instances docker"

# MS Agenda #
echo "Arrêt de la base de données du MS Agenda"
docker stop parentApp-agenda

# MS Evolution enfant #
echo "Arrêt de la base de données du MS Evolution enfatnt"
docker stop parentApp-evolution-enfant

# MS Gestion compte #
echo "Arrêt de la base de données du MS Gestion compte"
docker stop parentApp-gestion-compte

# MS Flux rss #
echo "Arrêt de la base de données du MS Flux rss"
docker stop parentApp-flux-rss

### Stop services ###
echo "Arrêt des services"

# Service Agenda #
echo "Arrêt du service Agenda"
sudo service parentApp-agenda stop

# Service Evolution enfant #
echo "Arrêt du service Evolution enfant"
sudo service parentApp-evolution-enfant stop

# Service Gestion compte #
echo "Arrêt du service Gestion compte"
sudo service parentApp-gestion-compte stop

# Service Flux rss #
echo "Arrêt du service Flux rss"
sudo service parentApp-flux-rss stop