path=$1
version=0.0.1-SNAPSHOT

echo "Création du dossier s'il n'existe pas"
mkdir $path -p
cd $path

### Clone repositories ###
echo "Clonage des répertoires"

# Client #
echo "Clonage du client"
git clone https://gitlab.com/parent-app/client.git
# MS Agenda #
echo "Clonage du MS Agenda"
git clone https://gitlab.com/parent-app/agenda
# MS Evolution enfant #
echo "Clonage du MS Evolution enfant"
git clone https://gitlab.com/parent-app/evolution-enfant
# MS Gestion compte #
echo "Clonage du MS Gestion compte"
git clone https://gitlab.com/parent-app/gestion-compte
# MS Flux RSS #
echo "Clonage du MS Flux RSS"
git clone https://gitlab.com/parent-app/flux-rss

### Create postgres instances and DB ###
echo "Création des instances docker de base de données"
# MS Agenda #
echo "Création de la base de données du MS Agenda"
sudo docker run --name parentApp-agenda -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=parentApp_agenda -d -p 5433:5432 postgres
# MS Evolution enfant #
echo "Création de la base de données du MS Evolution enfant"
sudo docker run --name parentApp-evolution-enfant -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=parentApp_croissance_enfant -d -p 5434:5432 postgres
# MS Gestion compte #
echo "Création de la base de données du MS gestion compte"
sudo docker run --name parentApp-gestion-compte -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=parentApp_gestion_compte -d -p 5432:5432 postgres
# MS Flux RSS #
echo "Création de la base de données du MS Flux RSS"
sudo docker run --name parentApp-flux-rss -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=parentApp_rss_feeds -d -p 5435:5432 postgres

### Install projects ###
echo "Installation des projets"

# Client #
echo "Installation du client"
cd $path/client
npm install

# MS Agenda #
echo "Installation du MS Agenda"
cd $path/agenda
mvn install

# MS Evolution enfant #
echo "Installation du MS Evolution enfant"
cd $path/evolution-enfant
mvn install

# MS Gestion compte #
echo "Installation du MS Gestion compte"
cd $path/gestion-compte
mvn install

# MS Flux RSS #
echo "Installation du MS Flux RSS"
cd $path/flux-rss
mvn install
