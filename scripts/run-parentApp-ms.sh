### Run docker instances ###
echo "Exécution des instances docker"

# MS Agenda #
echo "Exécution de la base de données du MS Agenda"
docker start parentApp-agenda

# MS Evolution enfant #
echo "Exécution de la base de données du MS Evolution enfant"
docker start parentApp-evolution-enfant

# MS Gestion compte #
echo "Exécution de la base de données du MS Gestion compte"
docker start parentApp-gestion-compte

# MS Flux rss #
echo "Exécution de la base de données du MS Flux rss"
docker start parentApp-flux-rss

### Run services ###
echo "Exécution des services"

# Service Agenda #
echo "Exécution du service Agenda"
sudo service parentApp-agenda start

# Service Evolution enfant #
echo "Exécution du service Evolution enfant"
sudo service parentApp-evolution-enfant start

# Service Gestion compte #
echo "Exécution du service Gestion compte"
sudo service parentApp-gestion-compte start

# Service Flux rss #
echo "Exécution du service Flux rss"
sudo service parentApp-flux-rss start