path=$1
servicePath=/etc/init.d
version=0.0.1-SNAPSHOT
serviceAgenda="$servicePath/parentApp-agenda"
serviceEvoEnfant="$servicePath/parentApp-evolution-enfant"
serviceGestionCompte="$servicePath/parentApp-gestion-compte"
serviceFluxRss="$servicePath/parentApp-flux-rss"

### Create services ###
echo "Création des services"

# MS Agenda #
echo "Création du service Agenda"
sudo rm $serviceAgenda
sudo ln -s "$path/agenda/target/parent-app-agenda-$version.jar" $serviceAgenda

# MS Evolution enfant #
echo "Création du service Evolution enfant"
sudo rm $serviceEvoEnfant
sudo ln -s "$path/evolution-enfant/target/parent-app-croissance-enfant-$version.jar" $serviceEvoEnfant

# MS Gestion compte #
echo "Création du service Gestion compte"
sudo rm $serviceGestionCompte
sudo ln -s "$path/gestion-compte/target/parent-app-gestion-compte-$version.jar" $serviceGestionCompte

# MS Flux RSS #
echo "Création du service Flux RSS"
sudo rm $serviceFluxRss
sudo ln -s "$path/flux-rss/target/parent-app-rss-feeds-$version.jar" $serviceFluxRss